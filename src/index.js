// yarn add express
const express = require("express");
const app = express();
app.use(express.json());
app.use(express.static("."));
const cors = require("cors");
app.use(cors());
// Tạo cái host
// domain
app.listen(8080);

const rootRoute = require("./routes/index");
app.use("/api", rootRoute);
