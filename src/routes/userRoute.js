// Tạo ra các API trong các đối tượng Route

// GET POST PUT DELETE

// GET user

const express = require("express");
const userRoute = express.Router();
const {
  getUser,
  createUser,
  updateUser,
  uploadUser,
  signUp,
  signIn,
} = require("../controller/userController");
const { upload } = require("../middlewares/upload");

// upload base 64
userRoute.post("/upload_base", upload.single("dataUpload"), uploadUser);
// POST  localhost:8080/api/user/upload=> upload:file
// POST upload
// userRoute.post("/upload", upload.single("dataUpload"), (req, res) => {
//   console.log(req.file); // lưu database
//   // lưu req.fle.filename=> 166666_test.jpeg
//   // response.send()=> domain/public/img/16161616_test.jpeg
// });
// GET user
userRoute.get("/getUser", getUser);
// POST user
userRoute.post("/createUser", createUser);
// UPDATE user
userRoute.put("/updateUser/:user_id", updateUser);

// SIGN UP
userRoute.post("/signup", signUp);
// SIGN IN
userRoute.get("/signin", signIn);
// GET Food relationship

module.exports = userRoute;
