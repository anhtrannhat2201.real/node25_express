// Tạo ra các API trong các đối tượng Route

// GET POST PUT DELETE

// GET user

const express = require("express");
const { getFood } = require("../controller/foodController");
const foodRoute = express.Router();

foodRoute.get("/getFood", getFood);
module.exports = foodRoute;
