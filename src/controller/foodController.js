const { successCode, errorCode, failCode } = require("../config/reponse");
const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const model = init_models(sequelize);
const getFood = async (req, res) => {
  let dataFood = await model.food_type.findAll({
    include: "foods",
  });
  successCode(res, dataFood, "Lay thanh cong");
};

// commonjs module
module.exports = { getFood };
