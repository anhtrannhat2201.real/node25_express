const { successCode, errorCode, failCode } = require("../config/reponse");
const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const model = init_models(sequelize);
const fs = require("fs");
const bcrypt = require("bcrypt");
const { fail } = require("assert");
// controller sẽ chứa request và response
// các hàm xử lý chức năng ở BE sẽ được chứa trong thư mục controllers
// R=> GET
const getUser = async (req, res) => {
  try {
    let data = await model.user.findAll();
    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    // console.log("error: ", error);
    errorCode(res, "Lỗi BackEnd");
  }
};

// C=>POST
const createUser = async (req, res) => {
  try {
    let { full_name, email, passWord } = req.body;
    let checkEmailObj = await model.user.findOne({
      where: {
        email,
      },
    });

    if (checkEmailObj) {
      failCode(res, { full_name, email, passWord }, "Email đã tồn tại");
    } else {
      let result = await model.user.create({ full_name, email, passWord });
      console.log("result: ", result);
      successCode(res, result, "Tạo user thành công");
    }
  } catch (error) {
    errorCode(res, "Loi BackEnd");
  }
};
// U => PUT
const updateUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let { full_name, email, passWord } = req.body;

    let checkUser = await model.user.findOne({
      where: {
        user_id,
      },
    });

    if (!checkUser) {
      failCode(res, "User không tồn tại");
    } else {
      let result = await model.user.update(
        { full_name, email, passWord },
        {
          where: {
            user_id,
          },
        }
      );
    }
    successCode(res, { user_id }, "Cap nhat thanh cong");
  } catch (error) {
    errorCode(res, "lỗi BackEnd");
  }
};
// D=>DELETE
// D=> .destroy()
const getTitle = (req, res) => {
  res.send("title");
};

const uploadUser = (req, res) => {
  if (req.file.size >= 400000) {
    fs.unlinkSync(process.cwd() + "/public/img/" + req.file.filename);
    res.send("Chỉ được phép upload 4MB");
    return;
  }
  if (req.file.mimetype != "image/jpeg" && req.file.mimetype != "image/jpg") {
    fs.unlinkSync(process.cwd() + "/public/img/" + req.file.filename);
    res.send("Sai định dạng");
    return;
  }
  fs.readFile(
    process.cwd() + "/public/img/" + req.file.filename,
    (err, data) => {
      //
      let dataBase = `data:${req.file.mimetype};base64,${Buffer.from(
        data
      ).toString("base64")}`;
      // lưu database

      // xử lý xóa file
      setTimeout(() => {
        fs.unlinkSync(process.cwd() + "/public/img/" + req.file.filename);
      }, 5000);
      res.send(dataBase);
    }
  );
};

// R:GET food
// include=> JOIN table
// SELECT * FROM food JOIN food_type
// include hỗ trợ dữ liệu object và list object
// const getFood = async (req, res) => {
//   let dataFood = await model.food_type.findAll({
//     include: "foods",
//   });
//   successCode(res, dataFood, "Lay thanh cong");
// };

// signup
const signUp = async (req, res) => {
  try {
    let { full_name, email, pass_word } = req.body;
    let passWordHash = bcrypt.hashSync(pass_word, 10);

    let checkEmail = await model.user.findOne({
      where: {
        email,
      },
    });
    if (checkEmail) {
      failCode(res, "Vui lòng nhập email khác", "Email đã tồn tại");
    } else {
      let data = await model.user.create({
        full_name,
        email,
        pass_word: passWordHash,
      });
      successCode(res, data, "Đăng ký thành công");
    }
  } catch (error) {
    errorCode(res, "Lỗi BackEnd");
  }
};
// sign in
const signIn = async (req, res) => {
  try {
    let { email, pass_word } = req.body;

    // mã hóa password
    let checkLogin = await model.user.findOne({
      where: {
        email,
        pass_word,
      },
    });

    if (checkLogin) {
      let checkPass = bcrypt.compareSync(pass_word, checkLogin.pass_word);
      if (checkPass) {
        successCode(res, checkLogin, "Login thanh cong");
      } else {
        failCode(res, "", "Mat khau khong dung");
      }
    } else {
      failCode(res, "", "Email khong dung!");
    }
  } catch (error) {
    errorCode(res, "Lỗi BackEnd");
  }
};
// commonjs module
module.exports = {
  getUser,
  createUser,
  updateUser,
  uploadUser,
  signUp,
  signIn,
};
